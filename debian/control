Source: mig
Section: devel
Priority: optional
Maintainer: GNU Hurd Maintainers <debian-hurd@lists.debian.org>
Uploaders: Samuel Thibault <sthibault@debian.org>
Homepage: http://www.gnu.org/software/hurd/microkernel/mach/mig/gnu_mig.html
Vcs-Browser: https://salsa.debian.org/hurd-team/mig
Vcs-Git: https://salsa.debian.org/hurd-team/mig.git
Build-Depends: dpkg-dev (>= 1.17.14), debhelper-compat (= 12), dh-exec,
 gnumach-dev (>= 2:1.8+git20231217~),
 flex, libfl-dev, bison
Rules-Requires-Root: no
Standards-Version: 4.6.0
#Note: XXX this file is generated from .in files, see debian/rules

Package: mig
Architecture: hurd-any
Multi-Arch: no
Depends: ${misc:Depends}, ${mig:host}
Description: GNU Mach Interface Generator
 This is the GNU distribution of the MIG, which is needed to compile
 the GNU C library, the GNU Hurd and GNU Mach.
 .
 This package provides a mig command that generates interfaces for the host
 architecture of this package.

Package: mig-for-host
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${mig:host}
Description: GNU Mach Interface Generator - metapackage for host
 This is the GNU distribution of the MIG, which is needed to compile
 the GNU C library, the GNU Hurd and GNU Mach.
 .
 Like the mig package, this produces interfaces for the host architecture of
 this package, but as a triplet-mig command which is possibly a cross-generator.

Package: mig-i686-gnu
Architecture: any-i386
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: mig (<< 1.8-7), gnumach-dev (<< 2:1.8+git20231217~), hurd-dev (<< 1:0.9.git20180129~)
Replaces: mig (<< 1.8-7)
Description: GNU Mach Interface Generator for i386 GNU/Hurd
 This is the GNU distribution of the MIG, which is needed to compile
 the GNU C library, the GNU Hurd and GNU Mach.
 .
 This package provides the generator targetting GNU arch
 i686-gnu for the i386 GNU/Hurd Debian port.

Package: mig-x86-64-gnu
Architecture: any-amd64
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: mig (<< 1.8-7), gnumach-dev (<< 2:1.8+git20231217~), hurd-dev (<< 1:0.9.git20180129~)
Replaces: mig (<< 1.8-7)
Description: GNU Mach Interface Generator for amd64 GNU/Hurd
 This is the GNU distribution of the MIG, which is needed to compile
 the GNU C library, the GNU Hurd and GNU Mach.
 .
 This package provides the generator targetting GNU arch
 x86-64-gnu for the amd64 GNU/Hurd Debian port.

Package: mig-aarch64-gnu
Architecture: any-arm64
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: mig (<< 1.8-7), gnumach-dev (<< 2:1.8+git20231217~), hurd-dev (<< 1:0.9.git20180129~)
Replaces: mig (<< 1.8-7)
Description: GNU Mach Interface Generator for arm64 GNU/Hurd
 This is the GNU distribution of the MIG, which is needed to compile
 the GNU C library, the GNU Hurd and GNU Mach.
 .
 This package provides the generator targetting GNU arch
 aarch64-gnu for the arm64 GNU/Hurd Debian port.
